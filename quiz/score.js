function scores() {
	var score = 0;
	var op1 = document.getElementsByName('op1');
	var op2 = document.getElementsByName('op2');
	
	var i = 0;
	for (i = 0; i < op1.length; i++) {
		if ((op1[i].value == '3') && (op1[i].checked == true)) {
			score++;
		}
	}
	
	for (i = 0; i < op2.length; i++) {
		if ((op2[i].value == '1') && (op2[i].checked == true)) {
			score++;
		}
	}

	var pres = document.getElementById('res');
	
	var resp = '';
	if (score < 2) {
		resp = 'answer';
	} else {
		resp = 'answers';
	}

	pres.innerHTML = 'You have got ' + score + ' correct ' + resp;

}
